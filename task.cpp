#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cmath>
using namespace std;
int * FillArrayWithNumbersFromDiapazon(int*, const int);
void PrintArray(int*, const int);
void PointNegativeElementsOnEvenPosition(int *, const int);
void ConvertNegativeElementsToSquare(int *, int *, const int);
void PointPositiveElementsOnOddPosition(int*, const int);
void ShiftEvenPosToTheBegining(int*, const int);
void SortArrayInDescendingOrder(int *, const int);

int main()
{
	srand(time(0));
	const int size = 10;
	int A[size] = {};
	int B[size] = {};
	*A = *FillArrayWithNumbersFromDiapazon(A, size);
	PrintArray(A, size);
	cout << "\n\nNegative elements on even positions : " << endl;
	PointNegativeElementsOnEvenPosition(A, size);

	cout << "\n\nPositive elements on odd positions : " << endl;
	PointPositiveElementsOnOddPosition(A, size);

	cout << "\n\nConvert negative elements to their square : " << endl;
	ConvertNegativeElementsToSquare(A, B, size);
	PrintArray(B, size);

	cout << "\n\nShift elements with even index to the begining : " << endl;
	ShiftEvenPosToTheBegining(B, size);
	PrintArray(B, size);

	cout << "\n\nSort elements in descending order : " << endl;
	SortArrayInDescendingOrder(B, size);
	PrintArray(B, size);

	cout << endl;
	return 0;
}
//-----------------------------------------------------------------
int * FillArrayWithNumbersFromDiapazon(int *a, const int _size)
{
	for (int i = 0; i < _size; i++)
	{
		a[i] = rand() % 26 - 14;
	}
	return a;
}
//-------------------------------------------------------------------
void PrintArray(int* a, const int _size)
{
	for (int i = 0; i < _size; i++)
		cout << a[i] << " ";
}
//-------------------------------------------------------------------
void PointNegativeElementsOnEvenPosition(int *a, const int _size)
{
	int _CountOfNegativeElements = 0;
	for (int i = 0; i < _size; i++)
	{
		if ((a[i] < 0) && (i % 2 == 0))
		{
			_CountOfNegativeElements++;
			cout << a[i] << " ";
		}
	}
	cout << "\nCount of negative elements on even position = " << _CountOfNegativeElements;

}
//-------------------------------------------------------------------
void ConvertNegativeElementsToSquare(int *a, int *b, const int _size)
{
	for (int i = 0; i < _size; i++)
	{
		if (a[i] < 0)
		{
			b[i] = pow(a[i], 2);
		}
		else
		{
			b[i] = a[i];
		}
	}
}
//---------------------------------------------------------------------
void PointPositiveElementsOnOddPosition(int *a, const int _size)
{
	int CountOfPositiveElements = 0;
	for (int i = 1; i < _size; i += 2)
	{
		if (a[i] > 0)
		{
			CountOfPositiveElements++;
			cout << a[i] << " ";
		}
	}
	cout << "\nCount of positive elements on odd position = " << CountOfPositiveElements;
}
//-------------------------------------------------------------------
void ShiftEvenPosToTheBegining(int* b, const int _size)
{
	int *masTemp = new int[_size];
	int i = 0, j = 0;
	while ((j < _size) && (i < _size / 2))
	{
		masTemp[i] = b[j];
		j += 2; i++;
	}
	i = _size / 2; j = 1;
	while (j < _size && i < _size)
	{
		masTemp[i] = b[j];
		j += 2; i++;
	}	
	for (int i = 0; i < _size; i++)
		b[i] = masTemp[i];
	delete[] masTemp;
}
//-------------------------------------------------------------------
void SortArrayInDescendingOrder(int *b, const int _size)
{
	int temp;
	for (int i = _size / 2; i <= _size; i++)
	{
		for (int j = _size - 1; j > i; j--)
		{
			if (b[j - 1] > b[j])
			{
				temp = b[j - 1];
				b[j - 1] = b[j];
				b[j] = temp;
			}
		}
	}
}